# Super-Siren #

## Description ##

[Siren](https://github.com/kevinswiber/siren) Hypermedia client library which process Siren HTTP responses into immutable Siren representations utilizing [superagent](https://github.com/visionmedia/superagent) for the underlying HTTP requests.